# debos-recipes
A collection of recipes for Debos.

## Preparing system
On a Debian system, install debos:
```bash
sudo apt install debos
```

## Running
Build the images like:

```bash
mkdir out
debos --artifactdir=out -t architecture:arm64 ospack-debian.yaml
debos --artifactdir=out image-rockpi-e.yaml
```
